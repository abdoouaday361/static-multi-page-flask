from flask_frozen import Freezer
from app import app  # replace 'your_flask_app' with the name of your Flask app module

freezer = Freezer(app)

if __name__ == '__main__':
    freezer.freeze()
