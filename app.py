from flask import Flask, render_template

app = Flask(__name__)

@app.route('/index.html')
def index():
    return render_template('index.html')

@app.route('/python.html')
def python():
    return render_template('python.html')

@app.route('/javascript.html')
def javascript():
    return render_template('javascript.html')

@app.route('/java.html')
def java():
    return render_template('java.html')

if __name__ == '__main__':
    app.run(debug=True)
